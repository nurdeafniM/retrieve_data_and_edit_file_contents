const {Transform} = require ('stream')
const fs = require ('fs')

String.prototype.capitalize = function(){
    return this.replace(/\s*\w/, x=>x.toUpperCase()).replace(/\.\s*\w/g, x=>x.toUpperCase())
}

const capitalize= new Transform({
    transform(chunk, encoding, callback){
        this.push(chunk.toString().capitalize())
        callback()
    }
})

var readStream= fs.createReadStream("./././example.txt")
var writeStream= fs.createWriteStream("./././example_output.txt")
readStream.pipe(capitalize).pipe(writeStream);