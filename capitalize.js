const fs = require('fs')
const readline = require('readline')

String.prototype.capitalize = function(){
    return this.charAt(0).toUpperCase()+ this.slice(1)
}
const ws = fs.createWriteStream('./example2.txt')
const rl = readline.createInterface({
    input : fs.createReadStream('./example.txt'),
    output : ws
})
rl.on('line', (line) =>{
    ws.write(line.capitalize())
})